#!/usr/bin/env python

from json import loads
from os import popen
from sys import argv,stderr
from subprocess import Popen, PIPE

def shell_run_get_stdout(cmd):
  return Popen(cmd, shell=True, close_fds=True, stdout=PIPE).stdout.readlines()

def ipc_query(req="command", msg=""):
  ans = shell_run_get_stdout("i3-msg -t " + req + " " +  msg)[0]
  return loads(ans)

def get_display_from_left(id):
  log("getting display for id %s" % id)
  return get_displays_ordered_from_left()[id]

def log(s):
    stderr.write(str(s) + "\n")

def get_displays_ordered_from_left():
  log("xrandr output %s" % "".join(shell_run_get_stdout("xrandr")))
  lines = shell_run_get_stdout("xrandr | grep ' connected.*[0-9]\\++[0-9]\\+'")
  log(lines)
  offset_display_list = []
  for line in lines:
    offset_display_list.append((get_horizontal_offset_from_xrandr_line(line), get_display_name_from_xrandr_line(line)))
  offset_display_list.sort()
  log(offset_display_list)
  return [d for (o,d) in offset_display_list]

def get_display_name_from_xrandr_line(line):
  return line.split(' ')[0]

def get_horizontal_offset_from_xrandr_line(line):
  for token in line.split(' '):
    if token.find('+') == -1:
      continue
    return int(token.split('+')[1])

def get_left_display():
  return get_display_from_left(0)

def get_right_display():
  return get_display_from_left(1)

def get_active_display():
  active_display = None
  for w in ipc_query(req="get_workspaces"):
    if w['focused']:
       return w['output']

GET_DISPLAY_MAP={'left' : get_left_display,
                 'right' : get_right_display,
                 'active' : get_active_display}

def check_args():
  # Usage & checking args
  if len(argv) != 3 or argv[2] not in ['left', 'right', 'active']:
     print "Usage: workspace-to-display.py name-of-workspace left/right/active"
     exit(-1)

def main():
  check_args()
  workspace = argv[1]
  target_display = GET_DISPLAY_MAP[argv[2]]()
  print ipc_query(msg="'workspace " + workspace + "; move workspace to output " + target_display + "; workspace " + workspace + "'")


if __name__ == "__main__":
    main()


